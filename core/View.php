<?php

class View {
    protected $data = [];
    protected $viewFile;

    public function __construct($viewFile) {
        $this->viewFile = $viewFile;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getViewFile() {
        return $this->viewFile;
    }

    public function display() {
        extract($this->data);
        include $this->viewFile;
    }
}