<?php

// 个简单的框架提供了基本的路由、控制器、模型和视图的分离。在实际开发中，你可能需要添加更多的功能，比如请求处理、响应处理、错误处理、数据库抽象层、用户认证、缓存机制等。这个框架只是一个起点，帮助你理解如何构建一个基本的PHP框架结构。


// 设置核心目录的相对路径
define('CORE_PATH', __DIR__ . '/../core');

// 引入Router.php
require_once CORE_PATH . '/Router.php';
require_once CORE_PATH . '/Controller.php';
require_once CORE_PATH . '/View.php';

// 创建路由实例
$router = new Router();

// 定义路由

// 添加路由规则

//$router->addRoute('GET', '/', 'HomeController', 'index');
$router->addRoute('GET', '/', 'HomeController@index');
//$router->addRoute('GET', '/about', 'AboutController@about');

echo 'index'.'<br>';

// 直接调用路由
$router->direct();