<?php

class Controller {
    protected $view;

    public function __construct($view) {
        $this->view = $view;
    }

    public function render($data = []) {
        $this->view->setData($data);
        include $this->view->getViewFile();
    }
}