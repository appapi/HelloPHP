<?php



class Router {
    protected $routes = array();

    public function addRoute($method, $uri, $callback) {
        $this->routes[$method][$uri] = $callback;
    }

    public function direct() {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        if (isset($this->routes[$method][$uri])) {
            call_user_func($this->routes[$method][$uri]);
        } else {
            $this->direct404();
        }
    }

    protected function direct404() {
        // 404 Not Found
        echo "404 Not Found";
        exit;
    }
}